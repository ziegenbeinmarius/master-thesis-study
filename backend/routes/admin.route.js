const express = require('express');
const router = express.Router();
const controller = require('../controllers/admin.controller');
const helper = require('../controllers/helper');

// Makes a user an admin
// router.post('/makeAdmin', helper.auth, controller.makeFirstAdmin);

// Deletes a participant using their ID
router.delete('/participant/:participantID', helper.auth, controller.deleteParticipant);

// Fetches all available study participants
router.get('/participants', helper.auth, controller.participantsList);

// Fetches all available study participants
router.get('/participantsFull', helper.auth, controller.participantsListFull);

module.exports = router;

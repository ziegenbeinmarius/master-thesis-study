const express = require('express');
const router = express.Router();
const controller = require('../controllers/participant.controller');

// Create study participant.
router.post('/participant', controller.createStudyParticipant);

module.exports = router;

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/User');

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function(username, password, done) {
        User.findOne({ email: username }, function (err, user) {
            if (err) { return done(err); }
            // Return if user not found in database
            if (!user) {
                return done(null, false, {
                    // message: 'User not found'
                    message: 'Username or password wrong'
                });
            }
            // Return if password is wrong
            if (!user.validPassword(password)) {
                return done(null, false, {
                    // message: 'Password is wrong'
                    message: 'Username or password wrong'
                });
            }
            // If credentials are correct, return the user object
            return done(null, user);
        });
    }
));

const passport = require('passport');
const User = require('../models/User');
const helper = require('./helper');

/**
 * Uses the credentials inside the body of the request to create a new user. After successfully
 * creating and saving the user, a token is sent as response which the user can use to
 * verify their identity.
 */
exports.register = (req, res) => {
    if (!req.body.email || !req.body.password) {
        res.status(401).json({
            message: "missing credentials"
        });
    } else {
        let newUser = new User();
        newUser.email = req.body.email;
        newUser.setPassword(req.body.password);

        return newUser.save().then((user) => {
            let token;
            token = user.generateJwt();
            res.status(200).json({
                token: token
            })
        }).catch(err => {
            res.status(500).json({
                message: err
            })
        });
    }
}

/**
 * Logs a user in by using the credentials to authenticate and responding with a token
 */
exports.login = (req, res) => {
    passport.authenticate('local', (err, user, info) => {
        let token;

        // If Passport throws/catches an error
        if (err) {
            res.status(404).json(err);
            return;
        }
        if (user) {
            token = user.generateJwt();
            res.status(200).json({
                token: token
            });
        } else {
            // If user is not found
            res.status(401).json(info);
        }
    })(req, res);
}

const User = require('../models/User');

/**
 * Finds a user using the userid stored in the received token
 */
exports.findUser = (req, res) => {
    return new Promise((resolve) => {
        if (!req.payload._id) {
            res.status(401).json({
                message: "UnauthorizedError: Private"
            });
        } else {
            User.findById(req.payload._id)
                .then(user => {
                    resolve(user);
                }).catch(this.handleError(res))
        }
    });
}

/**
 * Used to define the token for the routes
 */
const expressJwt = require('express-jwt');
// todo put secret into env
exports.auth = expressJwt({
    secret: "process.env.TOKEN_SECRET",
    algorithms: ['HS256'],
    userProperty: 'payload'
})

exports.handleError = (res, statusCode) => {
    statusCode = statusCode || 500;
    return (err) => {
        return res.status(statusCode).send(err);
    }
}

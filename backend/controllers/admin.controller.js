const StudyParticipant = require('../models/StudyParticipant');
const helper = require('./helper');

exports.makeFirstAdmin = function (req, res) {
    helper.findUser(req, res).then((user) => {
        user.role = 'admin';
        user.save();
        res.json(user);
    });
};

exports.deleteParticipant = function (req, res) {
    helper.findUser(req, res).then(user => {
        if (user.role === 'admin') {
            StudyParticipant.findByIdAndRemove(req.params.participantID, function (err, docs) {
                if (err) {
                    console.log(err)
                    res.status(400).json(err);
                } else {
                    console.log("Removed Participant : ", docs);
                    res.status(200).json(docs);
                }
            });
        } else {
            res.status(401).json(
                {message: "UnauthorizedError: Not available"});
        }
    });
};

exports.participantsList = function (req, res) {
    helper.findUser(req, res).then(user => {
        if (user.role === 'admin') {
            StudyParticipant.find()
                // .populate('postQuestionnaire')
                .then(studyParticipants => {
                    res.status(200).json(studyParticipants);
                })
        } else {
            res.status(401).json(
                {message: "UnauthorizedError: Not available"});
        }
    });
};

exports.participantsListFull = function (req, res) {
    helper.findUser(req, res).then(user => {
        if (user.role === 'admin') {
            StudyParticipant.find()
                .populate('postQuestionnaire')
                .then(studyParticipants => {
                    res.status(200).json(studyParticipants);
                })
        } else {
            res.status(401).json(
                {message: "UnauthorizedError: Not available"});
        }
    });
};

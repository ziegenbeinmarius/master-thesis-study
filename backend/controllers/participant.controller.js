const StudyParticipant = require("../models/StudyParticipant");
const PostQuestionnaire = require("../models/PostQuestionnaire");

exports.createStudyParticipant = function(req, res) {
    console.log(req.body)
    let newStudyParticipant = new StudyParticipant();
    if (req.body.groupNr)
        newStudyParticipant.groupNr = req.body.groupNr;
    if (req.body.secondsInTerms)
        newStudyParticipant.secondsInTerms = req.body.secondsInTerms;
    if (req.body.clickedBack)
        newStudyParticipant.clickedBack = req.body.clickedBack;

    let newPostQuestionnaire = new PostQuestionnaire();
    if (req.body.questionnaire.personalData.age)
        newPostQuestionnaire.personalData.age = req.body.questionnaire.personalData.age;

    if (req.body.questionnaire.personalData.gender)
        newPostQuestionnaire.personalData.gender = req.body.questionnaire.personalData.gender;

    // if (req.body.questionnaire.personalData.subject)
    // newPostQuestionnaire.personalData.subject = req.body.questionnaire.personalData.subject;
    //
    // if (req.body.questionnaire.personalData.subjectOther)
    //     newPostQuestionnaire.personalData.subjectOther = req.body.questionnaire.personalData.subjectOther;

    if (req.body.questionnaire.personalData.country)
        newPostQuestionnaire.personalData.country = req.body.questionnaire.personalData.country;

    for (const app of req.body.questionnaire.socialMediaData.apps) {
        newPostQuestionnaire.socialMediaData.apps.push(app)
    }

    if (req.body.questionnaire.socialMediaData.frequency)
        newPostQuestionnaire.socialMediaData.frequency = req.body.questionnaire.socialMediaData.frequency

    for (const reason of req.body.questionnaire.socialMediaData.reasonsOfUse ? req.body.questionnaire.socialMediaData.reasonsOfUse : []) {
        newPostQuestionnaire.socialMediaData.reasonsOfUse.push(reason)
    }
    if (req.body.questionnaire.socialMediaData.otherReason)
        newPostQuestionnaire.socialMediaData.otherReason = req.body.questionnaire.socialMediaData.otherReason

    for (const likert of req.body.questionnaire.socialMediaData.likertSocialMedia ? req.body.questionnaire.socialMediaData.likertSocialMedia : []) {
        newPostQuestionnaire.socialMediaData.likertSocialMedia.push(likert)
    }

    for (const comprehension of req.body.questionnaire.comprehensionData) {
        newPostQuestionnaire.comprehensionData.push(comprehension)
    }

    if (req.body.questionnaire.concerns)
        newPostQuestionnaire.concerns = req.body.questionnaire.concerns;

    for (const likert of req.body.questionnaire.termsData.likertTerms) {
        newPostQuestionnaire.termsData.likertTerms.push(likert)
    }
    for (const info of req.body.questionnaire.termsData.shareInformation) {
        newPostQuestionnaire.termsData.shareInformation.push(info)
    }

    if (req.body.questionnaire.likertHonesty.value)
        newPostQuestionnaire.likertHonesty.value = req.body.questionnaire.likertHonesty.value;

    if (req.body.questionnaire.feedback)
        newPostQuestionnaire.feedback = req.body.questionnaire.feedback;

    newPostQuestionnaire.save();

    newStudyParticipant.postQuestionnaire = newPostQuestionnaire;

    newStudyParticipant.save()
        .then((studyParticipant) => {
            res.json(studyParticipant);
        }).catch(handleError(res)); // catch any errors
};

function handleError(res, statusCode) {
    statusCode = statusCode || 500;
    return function(err) {
        return res.status(statusCode).send(err);
    };
}

let express = require('express'),
    mongoose = require('mongoose'),
    cors = require('cors'),
    passport = require('passport'),
    passportConfig = require('./config/passport'),
    bodyParser = require('body-parser'),
    dbConfig = require('../db/db.config');

const app = express();
const port = process.env.PORT || 3000;

// Connecting with mongo db
mongoose.Promise = global.Promise;
mongoose.connect(dbConfig.db, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then(() => {
      console.log('Database successfully connected')
    },
    error => {
      console.log('Database could not connect: ' + error)
    }
)

// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cors());

app.use(passport.initialize(undefined));

const admin = require('./routes/admin.route');
const authentication = require('./routes/authentication.route');
const study = require('./routes/participant.route');

// Make public static folder
app.use(express.static('../study/dist/study'));


app.get("/api", (req, res) => {
    res.send("Welcome API!");
});

app.use('/api', authentication);
app.use('/api/admin', admin);
app.use('/api/study', study);

app.get("/api/*", (req, res) => {
    res.send("Not implemented!");
});

app.get('/*', (req, res) => {
    res.status(200).sendFile(`/`, {root: '../study/dist/study'});
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

// error handler
app.use((err, req, res, next) => {
    console.error('Error: ' + err.message); // Log error message in our server's console
    if (!err.statusCode) err.statusCode = 500;
    res.status(err.statusCode).send(err.message);
});

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema
let PostQuestionnaire = new Schema({
    personalData: {
        age: String,
        gender: String,
        subject: String,
        subjectOther: {
            type: String,
            required: false
        },
        country: String
    },
    socialMediaData: {
        apps: [
            {
                selected: Boolean,
                name: String
            }],
        frequency: {
            type: Number,
            required: false
        },
        reasonsOfUse: {
            type: [
                {
                    selected: Boolean,
                    name: String
                }],
            required: false
        },
        otherReason: {
            type: String,
            required: false
        },
        likertSocialMedia: [
            {
                name: String,
                value: {
                    type: Number,
                    required: false
                }
            }],
    },
    comprehensionData: [
        {
            name: String,
            value: {
                type: Number,
                required: false
            }
        }
    ],
    concerns: String,
    termsData: {
        likertTerms: [
            {
                name: String,
                value: {
                    type: Number,
                    required: false
                }
            }],
        shareInformation: [
            {
                selected: Boolean,
                name: String
            }]

    },
    likertHonesty: {
        name: String,
        value: {
            type: Number,
            required: false
        }
    },
    feedback: String
}, {
    collection: 'postQuestionnaires'
})

module.exports = mongoose.model('PostQuestionnaire', PostQuestionnaire)

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema
let StudyParticipant = new Schema({
    groupNr: Number,
    secondsInTerms: Number,
    clickedBack: Boolean,
    postQuestionnaire: { type: Schema.Types.ObjectId, ref: 'PostQuestionnaire' },
}, {
    collection: 'studyParticipants'
})

module.exports = mongoose.model('StudyParticipant', StudyParticipant)

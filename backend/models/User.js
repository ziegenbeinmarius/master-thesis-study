const mongoose = require( 'mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const User = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    role: String,
    hash: String,
    salt: String
});

User.methods.setPassword = function(password){
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
};

User.methods.validPassword = function(password) {
    const hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
};

User.methods.generateJwt = function() {
    const expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);

    return jwt.sign({
        _id: this._id,
        email: this.email,
        exp: expiry.getTime() / 1000,
    }, "process.env.TOKEN_SECRET");
};

module.exports = mongoose.model('User', User)

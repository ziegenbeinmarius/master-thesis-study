import {Component} from '@angular/core';
import {NavigationEnd, Router, RouterEvent} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'study';
  menuEnabled = false;

  constructor(
    private router: Router
  ) {
    this.registerNoMenuPages();
  }

  registerNoMenuPages() {
    this.router.events.subscribe((event: RouterEvent) => {
      const noMenu = ['/demo', '/questionnaire', '/thanks'];
      if (event instanceof NavigationEnd && noMenu.some(substring => event.urlAfterRedirects.includes(substring))) {
        this.menuEnabled = false;
      } else if (event instanceof NavigationEnd && !noMenu.some(substring => event.urlAfterRedirects.includes(substring))) {
        this.menuEnabled = true;
      }
    });
  }
}

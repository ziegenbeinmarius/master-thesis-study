import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {WelcomeComponent} from './pages/welcome/welcome.component';
import {TermsPage1Component} from './pages/terms-page1/terms-page1.component';
import {TermsPage2Component} from './pages/terms-page2/terms-page2.component';
import {TermsPage3Component} from './pages/terms-page3/terms-page3.component';
import {QuestionnaireComponent} from './pages/questionnaire/questionnaire.component';
import {DemoComponent} from './pages/demo/demo.component';
import {ThanksComponent} from './pages/thanks/thanks.component';


const routes: Routes = [
  {path: '', redirectTo: 'welcome', pathMatch: 'full'},
  {path: 'welcome', component: WelcomeComponent},
  {path: 'terms1', component: TermsPage1Component},
  {path: 'terms2', component: TermsPage2Component},
  {path: 'terms3', component: TermsPage3Component},
  {path: 'demo', component: DemoComponent},
  {path: 'questionnaire', component: QuestionnaireComponent},
  {path: 'thanks', component: ThanksComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

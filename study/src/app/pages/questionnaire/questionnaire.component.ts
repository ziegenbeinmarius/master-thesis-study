import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ParticipantService} from '../../services/participant.service';
import {ApiService} from '../../services/api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.scss']
})
export class QuestionnaireComponent implements OnInit {

  personalDataForm: FormGroup;

  socialMediaFormValid = false;
  comprehensionFormValid = false;
  termsFormValid = false;

  otherReason = false;
  oneApp = false;
  noApp = false;

  public frequencyOptions = [
    {value: 1, id: 'Several times a day'},
    {value: 2, id: 'Once a day'},
    {value: 3, id: 'Several times a week'},
    {value: 4, id: 'Once a week'},
    {value: 5, id: 'less often'},
  ];

  public likertOptions = [
    {value: 1, id: 'Strongly Agree'},
    {value: 2, id: 'Agree'},
    {value: 3, id: 'Neutral'},
    {value: 4, id: 'Disagree'},
    {value: 5, id: 'Strongly Disagree'},
  ];

  public likertFrequencyOptions = [
    {value: 1, id: 'Always'},
    {value: 2, id: 'Often'},
    {value: 3, id: 'Regularly'},
    {value: 4, id: 'Rarely'},
    {value: 5, id: 'Never'},
  ];

  public comprehensionOptions = [
    {value: 1, id: 'True'},
    {value: 2, id: 'False'},
    {value: 3, id: 'Do not know'}
  ];

  constructor(
    public participantsService: ParticipantService,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.personalDataForm = this.formBuilder.group({
      age: ['', Validators.required],
      gender: ['', Validators.required],
      subject: ['', Validators.required],
      subjectOther: [''],
      country: [''],
    });
  }

  checkCheckBoxValue(event, selectedApp) {
    const item = this.participantsService.participant.questionnaire.socialMediaData.apps.find(app => app.name === selectedApp);
    item.selected = event.checked;
    this.oneApp = this.participantsService.participant.questionnaire.socialMediaData.apps.some(app => app.selected && app.name !== 'None');
    const selectedApps = this.participantsService.participant.questionnaire.socialMediaData.apps.filter(app => app.selected);
    this.noApp = selectedApps.filter(app => app.selected).length === 1
      && selectedApps.some(app => app.name === 'None');
  }

  checkCheckBoxValueReasons(event, selectedReason) {
    const item = this.participantsService.participant.questionnaire.socialMediaData.reasonsOfUse
      .find(reason => reason.name === selectedReason);
    item.selected = event.checked;
    if (item.name === 'Other') {
      this.otherReason = item.selected;
    }
  }

  checkCheckBoxValueTerms(event, selectedReason) {
    const item = this.participantsService.participant.questionnaire.termsData.shareInformation
      .find(reason => reason.name === selectedReason);
    item.selected = event.checked;
    // if (item.name === 'Other') {
    //   this.otherReason = item.selected;
    // }
  }

  checkComprehensionFormValidity() {
    let valid = true;
    for (const option of this.participantsService.participant.questionnaire.comprehensionData) {
      if (!option.value) {
        valid = false;
      }
    }
    if (valid) {
      this.comprehensionFormValid = valid;
    }
  }

  checkSocialMediaFormValidity() {
    let valid = true;
    for (const option of this.participantsService.participant.questionnaire.socialMediaData.likertSocialMedia) {
      if (!option.value) {
        valid = false;
      }
    }
    if (valid) {
      this.socialMediaFormValid = valid;
    }
  }

  checkTermsFormValidity() {
    let valid = true;
    for (const option of this.participantsService.participant.questionnaire.termsData.likertTerms) {
      if (!option.value) {
        valid = false;
      }
    }
    if (valid) {
      this.termsFormValid = valid;
    }
  }

  scrollToTop(px: number) {
    window.scroll(0, px);
  }

  sendAnswers() {
    this.apiService.createStudyParticipant()
      .subscribe(
        (res) => {
          // console.log(res);
          // console.log('Participant data sending successful!');
          this.router.navigate(['/thanks']);
        }, (error) => {
          // console.log(error);
        });
  }

}

import {Component, HostListener, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {ParticipantService} from '../../services/participant.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  private terms: number;
  public url = 'url("../../../assets/bg-1.jpg")';
  counter = 1;
  maxImage = 4;
  public screenHeight = 0;

  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
    private participantService: ParticipantService
  ) {
  }

  ngOnInit(): void {
    if (this.participantService.participant.groupNr) {
      this.terms = this.participantService.participant.groupNr;
    } else {
      this.route.queryParams
        .subscribe(params => {
            if (!params.t) {
              this.terms = Math.floor((Math.random() * 3) + 1);
            } else if (this.participantService.participant.groupNr) {
              this.terms = this.participantService.participant.groupNr;
            } else {
              this.terms = parseInt(params.t, 10);
            }
            this.participantService.participant.groupNr = this.terms;
          }
        );
    }

    setInterval(() => {
      this.url = `url("../../../assets/bg-${this.counter}.jpg")`;
      if (this.counter + 1 > this.maxImage) {
        this.counter = 1;
      } else {
        ++this.counter;
      }
    }, 8000);
    this.onResize();

    this.setAnimations();
  }

  /**
   * Navigates to the terms based on params.
   * @param event Used for stopping Propagation.
   */
  routeToTerms(event: Event) {
    event.stopPropagation();
    switch (this.terms) {
      case 1: {
        this.router.navigate(['/terms1']);
        break;
      }
      case 2: {
        this.router.navigate(['/terms2']);
        break;
      }
      case 3: {
        this.router.navigate(['/terms3']);
        break;
      }
      default: {
        const rand = Math.floor((Math.random() * 3) + 1);
        this.participantService.participant.groupNr = rand;
        const termPage = `/terms${rand}`;
        this.router.navigate([termPage]);
        break;
      }
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(): void {
    this.screenHeight = window.innerHeight - 1;
    // this.screenWidth = window.innerWidth;
  }

  setAnimations() {
    const section = document.querySelector('.section');
    section.classList.remove('section-transition');
    const observer = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          section.classList.add('section-transition');
          return;
        }

        section.classList.remove('section-transition');
      });
    });
    observer.observe(document.querySelector('.section-wrapper'));


    const section2 = document.querySelector('.section-2');
    section2.classList.remove('section-transition');
    const observer2 = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          section2.classList.add('section-transition');
          return;
        }

        section2.classList.remove('section-transition');
      });
    });
    observer2.observe(document.querySelector('.section-wrapper-2'));


    const section3 = document.querySelector('.section-3');
    section3.classList.remove('section-transition');
    const observer3 = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          section3.classList.add('section-transition');
          return;
        }

        section3.classList.remove('section-transition');
      });
    });
    observer3.observe(document.querySelector('.section-wrapper-3'));
  }

}

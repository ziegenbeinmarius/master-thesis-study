import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsPage1Component } from './terms-page1.component';

describe('TermsPage1Component', () => {
  let component: TermsPage1Component;
  let fixture: ComponentFixture<TermsPage1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsPage1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsPage1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

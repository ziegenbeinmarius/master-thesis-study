import {Component, OnInit} from '@angular/core';
import {ParticipantService} from '../../services/participant.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-terms-page1',
  templateUrl: './terms-page1.component.html',
  styleUrls: ['./terms-page1.component.scss']
})
export class TermsPage1Component implements OnInit {

  constructor(
    private participantService: ParticipantService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  /**
   * Navigate to the demo site after clicking back suggesting not accepting terms.
   */
  goBack() {
    this.participantService.participant.clickedBack = true;
    this.router.navigateByUrl('/demo');
  }
}

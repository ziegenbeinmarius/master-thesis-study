import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsPage3Component } from './terms-page3.component';

describe('TermsPage3Component', () => {
  let component: TermsPage3Component;
  let fixture: ComponentFixture<TermsPage3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsPage3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsPage3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

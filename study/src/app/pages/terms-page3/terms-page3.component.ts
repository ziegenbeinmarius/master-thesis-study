import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ParticipantService} from '../../services/participant.service';
import {Terms3Component} from '../../components/dialogs/terms3/terms3.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-terms-page3',
  templateUrl: './terms-page3.component.html',
  styleUrls: ['./terms-page3.component.scss']
})
export class TermsPage3Component implements OnInit {

  public acceptedTerms = false;

  constructor(
    public dialog: MatDialog,
    private participantService: ParticipantService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.openTermsDialog(true);
  }

  /**
   * Navigate to the demo site after clicking back suggesting not accepting terms.
   */
  goBack() {
    this.participantService.participant.clickedBack = true;
    this.router.navigateByUrl('/demo');
  }

  /**
   * Opens the dialog responsible fo
   */
  openTermsDialog(acceptButton: boolean) {
    const openDate = new Date();
    const dialogRef = this.dialog.open(Terms3Component, {
      data: {acceptButton},
      disableClose: true,
      maxWidth: '748px',
      autoFocus: false,
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.acceptedTerms = result.data;
      const closeDate = new Date();
      const secondsOpen = (closeDate.getTime() - openDate.getTime()) / 1000;
      this.participantService.participant.secondsInTerms += secondsOpen;
    });
  }

}

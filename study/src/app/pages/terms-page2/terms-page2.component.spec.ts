import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsPage2Component } from './terms-page2.component';

describe('TermsPage2Component', () => {
  let component: TermsPage2Component;
  let fixture: ComponentFixture<TermsPage2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsPage2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsPage2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

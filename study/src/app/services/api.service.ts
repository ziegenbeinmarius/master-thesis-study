import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Participant} from '../models/participant';
import {Observable, throwError} from 'rxjs';
import {ParticipantService} from './participant.service';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  BACKEND_URL = environment.BACKEND_URL;
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(
    private http: HttpClient,
    private participantsService: ParticipantService
  ) {
  }

  /**
   * Creates a new study participants using the extracted values and questionnaire.
   */
  createStudyParticipant(): Observable<Participant> {
    const url = `${this.BACKEND_URL}/study/participant`;
    return this.http.post<Participant>(url, this.participantsService.participant)
      .pipe(
        catchError(err => this.errorMgmt(err))
      );
  }

  /**
   * Responsible for handling the errors and building the error message.
   * @param error The received error
   */
  errorMgmt(error: HttpErrorResponse) {
    // let errorCode: number;
    let errorMessage: string;
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      // errorCode = error.status;
      errorMessage = error.error.message;
    }
    // console.log(errorMessage);
    return throwError(errorMessage);
  }
}

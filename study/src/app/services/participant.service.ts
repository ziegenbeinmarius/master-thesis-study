import {Injectable} from '@angular/core';
import {Participant} from '../models/participant';

@Injectable({
  providedIn: 'root'
})
export class ParticipantService {

  public participant = new Participant();

  constructor() {
  }
}

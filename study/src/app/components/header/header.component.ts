import {Component, OnInit} from '@angular/core';
import {ViewportScroller} from '@angular/common';
import {Router} from '@angular/router';
import {ParticipantService} from '../../services/participant.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public atTop = true;
  private terms: number;

  constructor(
    private viewportScroller: ViewportScroller,
    private router: Router,
    private participantsService: ParticipantService
  ) {
  }

  ngOnInit(): void {
    window.addEventListener('scroll', () => {
      this.atTop = window.scrollY <= 10;
    });
  }

  scrollToTop(): void {
    if (this.router.url === '/welcome') {
      this.viewportScroller.scrollToPosition([0, 0]);
    } else {
      this.router.navigate(['welcome']);
    }
  }

  /**
   * Navigates to the terms based on params.
   * @param event Used for stopping Propagation.
   */
  routeToTerms(event: Event) {
    event.stopPropagation();
    this.terms = this.participantsService.participant.groupNr;
    switch (this.terms) {
      case 1: {
        this.router.navigate(['/terms1']);
        break;
      }
      case 2: {
        this.router.navigate(['/terms2']);
        break;
      }
      case 3: {
        this.router.navigate(['/terms3']);
        break;
      }
      default: {
        this.router.navigate(['/terms1']);
        break;
      }
    }
  }
}

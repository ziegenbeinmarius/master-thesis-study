import {Component, OnInit} from '@angular/core';
import {ParticipantService} from '../../services/participant.service';
import {Terms1Component} from '../dialogs/terms1/terms1.component';
import {Terms2Component} from '../dialogs/terms2/terms2.component';
import {Terms3Component} from '../dialogs/terms3/terms3.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(
    private participantService: ParticipantService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
  }

  /**
   * Opens the correct terms dialog depending on the current URL location.
   */
  openTermsDialog() {
    if (this.participantService.participant.groupNr === 1) {
      this.openTerms1Dialog();
    } else if (this.participantService.participant.groupNr === 2) {
      this.openTerms2Dialog();
    } else if (this.participantService.participant.groupNr === 3) {
      this.openTerms3Dialog();
    }
  }

  /**
   * Opens the terms dialog for the first group.
   */
  openTerms1Dialog() {
    const openDate = new Date();
    const dialogRef = this.dialog.open(Terms1Component, {
      data: {},
      // width: '50%',
      maxWidth: '748px',
      autoFocus: false,
    });
    dialogRef.afterClosed().subscribe(() => {
      const closeDate = new Date();
      const secondsOpen = (closeDate.getTime() - openDate.getTime()) / 1000;
      this.participantService.participant.secondsInTerms += secondsOpen;
    });
  }

  /**
   * Opens the terms dialog for the second group.
   */
  openTerms2Dialog() {
    const openDate = new Date();
    const dialogRef = this.dialog.open(Terms2Component, {
      data: {acceptButton: false},
      // width: '50%',
      maxWidth: '748px',
      autoFocus: false,
    });
    dialogRef.afterClosed().subscribe(() => {
      const closeDate = new Date();
      const secondsOpen = (closeDate.getTime() - openDate.getTime()) / 1000;
      this.participantService.participant.secondsInTerms += secondsOpen;
    });
  }

  /**
   * Opens the terms dialog for the third group.
   */
  openTerms3Dialog() {
    const openDate = new Date();
    const dialogRef = this.dialog.open(Terms3Component, {
      data: {acceptedButton: false},
      // width: '50%',
      maxWidth: '748px',
      autoFocus: false,
    });
    dialogRef.afterClosed().subscribe(() => {
      const closeDate = new Date();
      const secondsOpen = (closeDate.getTime() - openDate.getTime()) / 1000;
      this.participantService.participant.secondsInTerms += secondsOpen;
    });
  }

}

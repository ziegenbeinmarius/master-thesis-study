import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsDefaultComponent } from './terms-default.component';

describe('TermsDefaultComponent', () => {
  let component: TermsDefaultComponent;
  let fixture: ComponentFixture<TermsDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsDefaultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

class DialogData {
  acceptButton: boolean;
}

@Component({
  selector: 'app-terms3',
  templateUrl: './terms3.component.html',
  styleUrls: ['./terms3.component.scss']
})
export class Terms3Component implements OnInit, AfterViewInit {

  menu = 1;
  public bottomOfTerms = false;
  @ViewChild('scrollViewTerms') scrollViewTerms: ElementRef;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dialogRef: MatDialogRef<Terms3Component>,
  ) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.scrollViewTerms.nativeElement.addEventListener('scroll', (event) => {
      const element = event.target;
      if (element.scrollHeight - element.scrollTop === element.clientHeight) {
        this.bottomOfTerms = true;
      }
    });
  }

  /**
   * Sets the number of the currently opened menu.
   * @param menuNumber Number indicating menu
   */
  setMenu(menuNumber: number) {
    this.menu = menuNumber;
  }

  /**
   * Closes the dialog and returns a positive result value.
   */
  accept() {
    this.dialogRef.close({data: true});
  }

  /**
   * Closes the dialog and returns a negative result value.
   */
  closeDialog() {
    this.dialogRef.close({data: false});
  }
}

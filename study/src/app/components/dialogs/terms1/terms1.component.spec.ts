import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Terms1Component } from './terms1.component';

describe('Terms1Component', () => {
  let component: Terms1Component;
  let fixture: ComponentFixture<Terms1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Terms1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Terms1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

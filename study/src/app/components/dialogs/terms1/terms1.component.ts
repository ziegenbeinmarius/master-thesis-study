import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {animate, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-terms1',
  templateUrl: './terms1.component.html',
  styleUrls: ['./terms1.component.scss']
})
export class Terms1Component implements OnInit, AfterViewInit {

  menu = 1;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: string,
    public dialogRef: MatDialogRef<Terms1Component>,
  ) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
  }

  /**
   * Sets the number of the currently opened menu.
   * @param menuNumber Number indicating menu
   */
  setMenu(menuNumber: number) {
    this.menu = menuNumber;
  }

  /**
   * Closes the dialog and returns a negative result value.
   */
  closeDialog() {
    this.dialogRef.close({data: false});
  }

}

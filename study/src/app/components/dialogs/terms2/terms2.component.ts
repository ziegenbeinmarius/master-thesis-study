import {AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

class DialogData {
  acceptButton: boolean;
}

@Component({
  selector: 'app-terms2',
  templateUrl: './terms2.component.html',
  styleUrls: ['./terms2.component.scss']
})
export class Terms2Component implements OnInit, AfterViewInit {

  menu = 1;
  public bottomOfTerms = false;
  @ViewChild('scrollViewTerms') scrollViewTerms: ElementRef;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dialogRef: MatDialogRef<Terms2Component>,
  ) {
  }

  ngOnInit(): void {
    // console.log(this.data);
  }

  ngAfterViewInit() {
    this.scrollViewTerms.nativeElement.addEventListener('scroll', (event) => {
      const element = event.target;
      if (element.scrollHeight - element.scrollTop === element.clientHeight) {
        this.bottomOfTerms = true;
      }
    });
  }

  /**
   * Sets the number of the currently opened menu.
   * @param menuNumber Number indicating menu
   */
  setMenu(menuNumber: number) {
    this.menu = menuNumber;
  }

  /**
   * Closes the dialog and returns a positive result value.
   */
  accept() {
    this.dialogRef.close({data: true});
  }

  /**
   * Closes the dialog and returns a negative result value.
   */
  closeDialog() {
    this.dialogRef.close({data: false});
  }
}

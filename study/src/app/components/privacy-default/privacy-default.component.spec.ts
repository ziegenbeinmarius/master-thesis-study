import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacyDefaultComponent } from './privacy-default.component';

describe('PrivacyDefaultComponent', () => {
  let component: PrivacyDefaultComponent;
  let fixture: ComponentFixture<PrivacyDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivacyDefaultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivacyDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

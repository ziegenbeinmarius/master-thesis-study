import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacyIconsComponent } from './privacy-icons.component';

describe('PrivacyIconsComponent', () => {
  let component: PrivacyIconsComponent;
  let fixture: ComponentFixture<PrivacyIconsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivacyIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivacyIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {Terms1Component} from '../dialogs/terms1/terms1.component';
import {Router} from '@angular/router';
import {Terms2Component} from '../dialogs/terms2/terms2.component';
import {Terms3Component} from '../dialogs/terms3/terms3.component';
import {ParticipantService} from '../../services/participant.service';

export interface TokenPayload {
  email: string;
  password: string;
  name?: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public credentials: TokenPayload = {
    email: '',
    name: '',
    password: ''
  };

  public registrationForm: FormGroup;
  @Input() accepted: boolean;

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private participantService: ParticipantService
  ) {
  }

  // todo include Validators again
  ngOnInit(): void {
    this.registrationForm = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([
          Validators.required,
          // tslint:disable-next-line:max-line-length
          // Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
        ]
      ))
      ,
      password: new FormControl(null, Validators.compose([
        // Validators.minLength(8),
        Validators.required,
        // Checks for Upper, lower case, a number and a special character (More can be added into [=|#*.!@$%^&(){}\[\]\\:;<>,?~_+-/])
        // tslint:disable-next-line:max-line-length
        // Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[*.!@$%^&(){}\[\]:;<>,?\/~_+\-=|]).{8,32}$/)
      ])),
      confirmPassword: new FormControl('', Validators.required),
      acceptedTerms: new FormControl('', Validators.compose([
        Validators.requiredTrue
      ]))
    }, {
      // validators: this.matchingPasswords.bind(this)
    });
  }

  /**
   * Checks if the password and confirmPassword are equal inside the formGroup.
   * @param formGroup The attached form group
   */
  matchingPasswords(formGroup: FormGroup) {
    const {value: password} = formGroup.get('password');
    const {value: confirmPassword} = formGroup.get('confirmPassword');
    return password === confirmPassword ? null : {mismatchedPasswords: true};
  }

  /**
   * Navigate to the demo site after registering.
   */
  register() {
    this.router.navigateByUrl('/demo');
  }

  /**
   * Opens the correct terms dialog depending on the current URL location.
   */
  openTermsDialog() {
    if (this.router.url === '/terms1') {
      this.openTerms1Dialog();
    } else if (this.router.url === '/terms2') {
      this.openTerms2Dialog();
    } else if (this.router.url === '/terms3') {
      this.openTerms3Dialog();
    }
  }

  /**
   * Opens the terms dialog for the first group.
   */
  openTerms1Dialog() {
    const openDate = new Date();
    const dialogRef = this.dialog.open(Terms1Component, {
      data: {},
      // width: '50%',
      maxWidth: '748px',
      autoFocus: false,
    });
    dialogRef.afterClosed().subscribe(() => {
      const closeDate = new Date();
      const secondsOpen = (closeDate.getTime() - openDate.getTime()) / 1000;
      this.participantService.participant.secondsInTerms += secondsOpen;
    });
  }

  /**
   * Opens the terms dialog for the second group.
   */
  openTerms2Dialog() {
    const openDate = new Date();
    const dialogRef = this.dialog.open(Terms2Component, {
      data: {acceptedButton: false},
      // width: '50%',
      maxWidth: '748px',
      autoFocus: false,
    });
    dialogRef.afterClosed().subscribe(() => {
      const closeDate = new Date();
      const secondsOpen = (closeDate.getTime() - openDate.getTime()) / 1000;
      this.participantService.participant.secondsInTerms += secondsOpen;
    });
  }

  /**
   * Opens the terms dialog for the third group.
   */
  openTerms3Dialog() {
    const openDate = new Date();
    const dialogRef = this.dialog.open(Terms3Component, {
      data: {acceptedButton: false},
      // width: '50%',
      maxWidth: '748px',
      autoFocus: false,
    });
    dialogRef.afterClosed().subscribe(() => {
      const closeDate = new Date();
      const secondsOpen = (closeDate.getTime() - openDate.getTime()) / 1000;
      this.participantService.participant.secondsInTerms += secondsOpen;
    });
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsIconsComponent } from './terms-icons.component';

describe('TermsIconsComponent', () => {
  let component: TermsIconsComponent;
  let fixture: ComponentFixture<TermsIconsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

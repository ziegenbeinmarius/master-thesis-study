import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MaterialModule} from './shared/material/material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {WelcomeComponent} from './pages/welcome/welcome.component';
import {HeaderComponent} from './components/header/header.component';
import {Terms1Component} from './components/dialogs/terms1/terms1.component';
import {TermsPage1Component} from './pages/terms-page1/terms-page1.component';
import {TermsPage2Component} from './pages/terms-page2/terms-page2.component';
import {TermsPage3Component} from './pages/terms-page3/terms-page3.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RegisterComponent} from './components/register/register.component';
import {TermsDefaultComponent} from './components/terms-default/terms-default.component';
import {QuestionnaireComponent} from './pages/questionnaire/questionnaire.component';
import {Terms2Component} from './components/dialogs/terms2/terms2.component';
import {Terms3Component} from './components/dialogs/terms3/terms3.component';
import {TermsIconsComponent} from './components/terms-icons/terms-icons.component';
import { FooterComponent } from './components/footer/footer.component';
import { DemoComponent } from './pages/demo/demo.component';
import { SlidePanelComponent } from './components/slide-panel/slide-panel.component';
import { PrivacyDefaultComponent } from './components/privacy-default/privacy-default.component';
import { PrivacyIconsComponent } from './components/privacy-icons/privacy-icons.component';
import { ThanksComponent } from './pages/thanks/thanks.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    HeaderComponent,
    Terms1Component,
    TermsPage1Component,
    TermsPage2Component,
    TermsPage3Component,
    RegisterComponent,
    TermsDefaultComponent,
    QuestionnaireComponent,
    Terms2Component,
    Terms3Component,
    TermsIconsComponent,
    FooterComponent,
    DemoComponent,
    SlidePanelComponent,
    PrivacyDefaultComponent,
    PrivacyIconsComponent,
    ThanksComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

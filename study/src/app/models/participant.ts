export class Participant {
  groupNr: number;
  secondsInTerms = 0;
  clickedBack = false;
  questionnaire: Questionnaire = new Questionnaire();
}

export class Questionnaire {
  personalData: {
    age: string,
    gender: string;
    subject: string;
    subjectOther?: string;
    country: string;
  } = {age: '', gender: '', subject: '', country: ''};

  socialMediaData: {
    apps: { selected: boolean, name: string }[];
    frequency?: string;
    reasonsOfUse?: { selected: boolean, name: string }[];
    otherReason?: string;
    likertSocialMedia: { name: string, value?: string }[];
  } = {
    apps: [
      {selected: false, name: 'Facebook'},
      {selected: false, name: 'Facebook Messenger'},
      {selected: false, name: 'Snapchat'},
      {selected: false, name: 'TikTok'},
      {selected: false, name: 'YouTube'},
      {selected: false, name: 'Twitter'},
      {selected: false, name: 'WhatsApp'},
      {selected: false, name: 'Instagram'},
      {selected: false, name: 'Reddit'},
      {selected: false, name: 'Pinterest'},
      {selected: false, name: 'None'}
    ],
    reasonsOfUse: [
      {selected: false, name: 'Stay up to date with current events'},
      {selected: false, name: 'Find funny and entertaining content'},
      {selected: false, name: 'Fill spare time'},
      {selected: false, name: 'Stay in touch with friends and family'},
      {selected: false, name: 'Share photos or videos'},
      {selected: false, name: 'Other'}
    ],
    likertSocialMedia: [
      {
        name: 'I share pictures with my friends and family using social media services.'
      }, {
        name: 'I share pictures with the community of my social media services.'
      }, {
        name: 'I publicly share pictures using social media services.'
      }
    ]
  };

  comprehensionData: { name: string, value?: string }[] = [
    {
      name: 'people under 18 years old can access our service.'
    },
    {
      name: 'we are not collecting information about your geo-location.'
    },
    {
      name: 'we are sharing information with third-parties.'
    },
    {
      name: 'we get the copyright of your uploaded photographs.'
    },
    {
      name: 'we can put restrictions regarding storage size on your account.'
    },
    {
      name: 'we can not share your personal information with law enforcement agencies.'
    }];

  concerns: string;

  termsData: {
    likertTerms: { name: string, value?: string }[],
    shareInformation: { selected: boolean, name: string }[];
  } = {
    likertTerms: [
      {name: 'I read terms of service and privacy policies.'},
      {name: 'I read terms of service and privacy policies thoroughly.'},
      {name: 'Adding privacy icons* improves my understanding of privacy policies.'},
      {name: 'I worry about my online data privacy.'},
    ],
    shareInformation: [
      {selected: false, name: 'Full Name'},
      {selected: false, name: 'Email'},
      {selected: false, name: 'Phone Number'},
      {selected: false, name: 'Location Data'},
      {selected: false, name: 'Racial or ethnic data'},
      {selected: false, name: 'Political opinions'},
      {selected: false, name: 'Religious or philosophical beliefs'},
      {selected: false, name: 'Trade union membership'},
      {selected: false, name: 'Genetic data'},
      {selected: false, name: 'Biometric data'},
      {selected: false, name: 'Health data'},
      {selected: false, name: 'Sex life or sexual orientation'},
      {selected: false, name: 'Income'},
      {selected: false, name: 'Insurance provider'},
      {selected: false, name: 'Private photos'},
      {selected: false, name: 'Access to your contacts list'},
      {selected: false, name: 'Browsing history'},
    ]
  };

  likertHonesty: { name: string, value?: string } = {name: 'I have honestly read the terms of service and privacy policy of this service.'};
  feedback: string;
}
